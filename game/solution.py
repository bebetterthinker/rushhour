from typing import Optional
from time import sleep
from threading import Thread
from ete3 import Tree, TreeStyle, NodeStyle, TextFace
from game.game import Game
from game.vehicle import *
from gui.board import Board

WINNING_COLOR = Colors.LIGHTGREEN
COLLISION_COLOR = Colors.RED
ALREADY_SEEN_COLOR = Colors.BLACK
ROOT_COLOR = Colors.CREAM
NODE_COLOR = Colors.CREAM


class Node:
    """
    This class represent a graph node.
    If parent is None, this node is the first of the graph : it's a root.
    By default, a node has no child : it's a leaf.
    Each node save a game state which represent the position of all vehicles with precedent nodes movements.
    """

    def __init__(self, name: str, gameState: list[Vehicle], parent: 'Node' = None):
        self._name = name
        self._parent = parent
        self._gameState = gameState
        self._childsList = []
        self._depth = 0

        self._nodeStyle = NodeStyle()
        self._nodeStyle["size"] = 40
        self._color = ""
        self.setColor(NODE_COLOR)

    def addChild(self, child: 'Node' = None):
        if child is None:
            return

        self._childsList.append(child)

    def getNodeStyle(self) -> NodeStyle:
        return self._nodeStyle

    def removeChild(self, child: 'Node' = None):
        if child:
            self._childsList.remove(child)

    def getChildAt(self, index: int = 0) -> 'Node':
        if not self.isLeaf():
            return self._childsList[index]

    def getDepth(self) -> int:
        return self._depth

    def setDepth(self, newDepth: int):
        if newDepth is None:
            return

        self._depth = newDepth

    def getChildsList(self) -> list['Node']:
        return self._childsList

    def getDescendants(self) -> Optional[list['Node']]:
        if self.isLeaf():
            return None
        else:
            res = self._childsList.copy()
            for child in self._childsList:
                descendants = child.getDescendants()
                if descendants is not None:
                    res.extend(descendants)
            return res

    def getGameState(self) -> list[Vehicle]:
        return self._gameState

    def getParent(self) -> 'Node':
        return self._parent

    def setParent(self, parent: 'Node' = None):
        self._parent = parent

    def getName(self) -> str:
        return self._name

    def getColor(self) -> str:
        return self._color

    def setName(self, newName: str = ""):
        self._name = newName

    def setColor(self, newColor: str):
        """
        Set the color of this node to the newNode parameter if its not None.
        Also actualize the node style in the graph representation.
        """
        if newColor is None:
            return

        self._color = newColor
        self._nodeStyle["fgcolor"] = newColor
        self._nodeStyle["vt_line_color"] = newColor
        self._nodeStyle["hz_line_color"] = newColor
        # self.nodeStyle["bgcolor"] = newColor # We cannot read anything with this scheme
        if self._color == WINNING_COLOR:
            self._nodeStyle["vt_line_width"] = 5
            self._nodeStyle["hz_line_width"] = 5
        else:
            self._nodeStyle["vt_line_width"] = 2
            self._nodeStyle["hz_line_width"] = 2

    def toGraphString(self) -> str:
        if self.isLeaf():
            return self._name
        else:
            text = "("
            for child in self._childsList:
                if self._childsList.index(child) == len(self._childsList) - 1:
                    text += child.toGraphString()
                else:
                    text += child.toGraphString() + ", "
            text += ")"
            return text

    def isLeaf(self) -> bool:
        return self._childsList == []

    def isRoot(self) -> bool:
        return self._parent is None


class Graph:
    """
    Each node represent a state of the game with only one car that moved
    The first Node is the current state of the game : the start situation.
    """

    def __init__(self, name: str, rootNode: Node, board: Board):
        self._name = name
        self._rootNode = rootNode
        self._board = board

        self._exitAsRectangle = self._board.getExitCellAsRectangle()
        self._exitAsRectangle.x -= self._board.getCellSize()[0]  # Make the previous x cell (in front of the exit) considered as an exit position

    def getRootNode(self) -> Node:
        return self._rootNode

    def getNodeList(self) -> Optional[list[Node]]:
        if self._rootNode is None:
            return

        return self._rootNode.getDescendants()

    @staticmethod
    def addNode(name, parentNode, gameState):
        newNode = Node(name, gameState, parentNode)
        parentNode.addChild(newNode)
        return newNode

    def solve(self):
        boardsToCheck = []
        boardsVisited = set()
        currentBoard = self._rootNode.getGameState()

        boardsToCheck.append([[], currentBoard, self._rootNode])
        boardsVisited.add(hash(str(currentBoard)))
        while len(boardsToCheck) > 0:
            moves, currentBoard, parentNode = boardsToCheck.pop(0)  # Take the first board and remove it

            if self.gameSolved(currentBoard):
                parentNode.setColor(WINNING_COLOR)
                print("solution trouvée")
                print(str(len(boardsVisited)) + " plateaux différents générés")
                return moves

            for [nextBoardMoves, nextBoard, nextBoardNode] in self.getNextBoards(currentBoard, parentNode):
                if hash(str(nextBoard)) not in boardsVisited: # I use the hash(str(Vehicle)) method in order to compare the nextBoard with already seen boards even if the two objects are no the same.
                    boardsToCheck.append([moves + nextBoardMoves, nextBoard, nextBoardNode])
                    boardsVisited.add(hash(str(nextBoard)))
                else:
                    nextBoardNode.setColor(ALREADY_SEEN_COLOR)

        print("Impossible de résoudre ce plateau")

    def getNextBoards(self, currentBoard, parentNode):
        nextBoards = []

        for vehicle in currentBoard:
            for orientation in Orientations.ORIENTATIONS_LIST:
                if vehicle.canMove(orientation, currentBoard):
                    # print(vehicle.getName() + " can move " + str(vehicle.getOrientationName(orientation)))

                    boardCopy = [v.copy() for v in currentBoard]
                    vehicleToMove = next(filter(lambda v: v.getId() == vehicle.getId(), boardCopy))
                    vehicleToMove.move(orientation)
                    boardNode = self.addNode(vehicle.getName() + " move " + str(vehicle.getOrientationName(orientation)), parentNode, boardCopy)
                    nextBoards.append([[[vehicle.getId(), vehicle.getName(), vehicle.getOrientationName(orientation)]], boardCopy, boardNode])

                    if str(boardCopy) == str(currentBoard):
                        # This situation is not supposed to exists
                        print("NO CHANGE")
                        print("Try to move " + vehicle.getName() + " direction " + str(orientation))

        # print(" -> " + str(nextBoards))
        return nextBoards

    def gameSolved(self, vehiclesList):
        redCar = next(filter(lambda vehicle: vehicle.getColor() == Colors.RED, vehiclesList))
        return redCar.rect.colliderect(self._exitAsRectangle)

    def toGraphString(self) -> str:
        return self._rootNode.toGraphString()

    def draw(self):
        t = Tree(self.toGraphString() + ";")

        ts = TreeStyle()
        ts.mode = "c"
        ts.show_leaf_name = False
        ts.show_branch_length = True
        ts.show_branch_support = False
        # ts.arc_start = -180
        # ts.arc_span = 180
        ts.title.add_face(TextFace(self._name, fsize=20), column=0)

        nodeList = self.getNodeList()
        nodeIndex = -1
        for node in t.traverse():
            if nodeIndex > -1:
                node.set_style(nodeList[nodeIndex].getNodeStyle())  # Set the style of each node
                node.dist = nodeList[nodeIndex].getDepth()  # Set the distance to parent
                node.add_feature("gameState", nodeList[nodeIndex].getGameState())
                node.add_face(TextFace(nodeList[nodeIndex].getName(), fsize=10), column=0)
                # node.add_face(TextFace("", fsize=15), column=0)

            else:
                # This is the root
                rootStyle = NodeStyle()
                rootStyle["size"] = 40
                rootStyle["fgcolor"] = ROOT_COLOR
                rootStyle["vt_line_color"] = ROOT_COLOR
                rootStyle["hz_line_color"] = ROOT_COLOR
                rootStyle["vt_line_width"] = 2
                rootStyle["hz_line_width"] = 2
                node.set_style(rootStyle)

            nodeIndex += 1

        # t.show(tree_style=ts)
        t.render("Graph.pdf", tree_style=ts)


class Solution:

    def __init__(self, game: Game, board: Board):
        self._game = game
        self._board = board
        self._graph = None
        self._victoryMoves = None

    def findMaxPlacementsPossibilities(self):
        """
        Multiply the result by the number of placements possibles for each vehicle
        For example, if we have 3 cars of 2 cells and 4 trucks of 3 cells on a 6*6 board, we will have:
          - 5 placements for each car
          - 4 placements for each truck
        This makes 5*5*5 * 4*4*4*4 = 5³*4⁴ = 32000. The number of possibilities for each type of vehicle is multiplied because at each vehicle placement corresponds a new placement of each other vehicle with its proper set of possible placements..
        """

        result = 1
        boardVerticalCellSize, boardHorizontalCellSize = self._board.getCellNumber()

        for vehicle in self._game.getVehiclesList().sprites():
            vehicleCellSize = vehicle.getCellSize()

            if vehicle.isVertical():
                result *= (boardVerticalCellSize - vehicleCellSize) + 1  # Corresponds to cells that are not covered by this vehicle (and probably that the vehicule can move on) + 1 for the current position of the vehicle.
            else:
                result *= (boardHorizontalCellSize - vehicleCellSize) + 1  # Corresponds to cells that are not covered by this vehicle (and probably that the vehicule can move on) + 1 for the current position of the vehicle.

        print("Nombre de positions pour les véhicules: " + str(result))
        return result

    def createGraph(self):
        # noinspection PyTypeChecker
        self._graph = Graph("Résolution par brut force", Node("root", self._game.getVehiclesList().sprites()), self._board)
        self._victoryMoves = self._graph.solve()
        # TODO:
        #   Séparer le méthode de résolution de celle pour gérer un grpahe de sorte à proposer plusieurs méthodes différentes.
        print("Solution en " + str(len(self._victoryMoves)) + " coups")
        # print(self._victoryMoves)
        # graph.draw()

    def getVictoryMoves(self):
        return self._victoryMoves

    def animateSolution(self):
        # TODO:
        #   Ajouter un slider pour régler le temps de l'animation
        #   Remettre le jeu à se situation de départ (méthode reset qui remet la liste à un attribut conservant la situation initiale créée lors de l'ajout des véhicules et donc lors de l'ajout des véicules, le faire dans 2 listes ? ou en utilisant la gameNode root)
        # self._game.setVehiclesList(self._graph.getRootNode().getGameState()) # Reset the game the the initial state
        sleep(0.2)
        for vId, vehicle, move in self._victoryMoves:
            vehicleToMove = next(filter(lambda v: v.getId() == vId, self._game.getVehiclesList().sprites()))
            vehicleToMove.move(move)
            sleep(1)
