from game.vehicle import *


class Truck(Vehicle):

    def __init__(self, vehicleId: int, name: str, color: str, board: Board, orientation: Orientations, cellPosition: tuple[int, int]):
        if color == Colors.RED:
            raise Exception("A truck cannot be red because the only vehicule that can be red is the car that need to be released")

        Vehicle.__init__(self, vehicleId, name, color, 3, board, orientation, cellPosition)
        self.drawVehicle()

    def drawVehicle(self):
        # Creation of the vehicle surface
        self.image = pygame.Surface([self._board.getCellWidth() * self._cellSize, self._board.getCellHeight()])  # The name of this attribute need to be image to correspond to the standards use by pygame.Sprite interface
        self.image.fill("Black")
        self.image.set_colorkey("Black")

        # Draw the truck load in the surface
        truckLoad = pygame.Rect(Vehicle.WIDTH_MARGIN / 2, Vehicle.HEIGHT_MARGIN / 2, self._vehicleWidth - self._board.getCellWidth(), self._vehicleHeight)
        pygame.draw.rect(self.image, self._color, truckLoad, 0, 10)  # The vehicle load as a rectangle
        pygame.draw.rect(self.image, Colors.BLACK if self._color != Colors.BLACK else "White", truckLoad, 3, 10)  # The vehicle rectangle outline

        truckHead = pygame.Rect(truckLoad.x + ((self._cellSize - 1) * self._board.getCellWidth()), truckLoad.y, self._board.getCellWidth() - Vehicle.WIDTH_MARGIN, self._vehicleHeight)
        pygame.draw.rect(self.image, self._color, truckHead, 0, 10)  # The vehicle head as a rectangle
        pygame.draw.rect(self.image, Colors.BLACK if self._color != Colors.BLACK else "White", truckHead, 3, 10)  # The vehicle head rectangle outline
        pygame.draw.line(self.image, Colors.BLACK, truckLoad.midright, truckHead.midleft, 6)  # The vehicle holding bar between head and load

        self.drawHeadLights()
        self.display()

    def copy(self):
        return Truck(self._id, self._name, self._color, self._board, self._orientation, self._cellPosition)
