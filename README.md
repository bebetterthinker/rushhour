<center><h1>Rush Hour</h1></center>

### Français

L'objectif de ce mini projet est de réfléchir à la résolution automatique d'un problème posé dans le jeu Rush Hour.

En temps normal, le jeu se présente sous la forme d'un plateau (une sorte de parking) sur lequel des camions et voitures sont placés de certaines façons, illustrées sur des cartes. Le but du jeu est de faire s'échapper la voiture de couleur rouge du plateau tout en sachant que celui-ci ne possède qu'une seule sortie de la largeur d'un véhicule. Le joueur devra donc déplacer les véhicules bloquant la route de la voiture rouge pour parvenir à cet objectif.

Dans la version que j'ai reproduite, j'ai d'abord modélisé le jeu sous la forme d'un tableur excel pour manipuler les véhicules et commencer à élaborer une stratégie de résolution du problème. J'ai ensuite modélisé le jeu en python grâce à la librairie pygame : plateau, véhicules (camions et voiture), déplacements, collisions entre véhicules et avec les bordures du plateau, situation de victoire. Il ne restait plus qu'à tester des algortihmes de résolution pour des problèmes de ce genre : je me suis orienté vers le domaine des graphes.

L'objectif de cette version reste le même que dans le jeu original (faire s'achapper la voiture rouge) mais la solution doit :
- Respecter un nombre de déplacements minimum des véhicules,
- Être trouvée en un temps raisonnable pour un ordinateur (pas plus d'une seconde),
- Exister quelque soit la configuration du plateau dans sa taille de base (6x6 cases).

Dans la version "étendue" de mon programme, j'aurai souhaité implémenter :
- Une résolution quelque soit la taille du plateau,
- Une génération du placement des voitures selon différentes difficultées (plaçant plus ou moins de véhicules),
- Un autre algorithme de résolution pour comparer les résultats en terme de performances (temps, utilsiation CPU, utilisation mémoire RAM),
- Une reconnaissance des plateaux non résolvables (de par la position des véhicules).

J'ai bel et bien trouvé une solution qui fonctionne : celle-ci consisite à générer tous les plateaux de jeu possibles avec, pour chaque plateau, un seul véhicule ayant été déplacé. Pour chaque plateau ainsi construit, on vérifie que celui-ci représente bien une situation valide (un véhicule cherchant à se déplacer sur un autre véhicule n'est pas une situation valide par exemple) et que le plateau n'est pas déjà été généré. On continu ainsi en faisant attention à ce que chaque nouveau plateau valide généré soit relié au plateau précédent (celui à partir duquel ce nouveau plateau a été généré via le déplacement d'un véhicule) et l'on ne s'arrête que lorsqu'un plateau représentant une situation de victoire est généré (voiture rouge sortie du plateau) ou qu'il n'y a plus de plateau générable (tous les déplacements de tous les véhicules ont été représentés sous forme de plateau mais aucun n'a créé une situation de victoire).
Si un plateau représentant une vitoire est trouvé, on identifie les mouvements (appliqués aux véhicules) qui ont conduit à ce plateau de vicoire en remontant de plateau généré en plateau généré jusqu'au plateau d'origine de la partie.

### Anglais

The objective of this mini-project is to think about the automatic resolution of a problem posed in the game called Rush Hour.

Ordinarily, the game takes the form of a board (a kind of parking) on which trucks and cars are placed in certain ways, illustrated on cards. The goal of the game is to make the red car escape from the board, knowing that the board has only one exit of the width of a vehicle. The player will have to move the vehicles blocking the road of the red car to achieve this goal.

In the version I reproduced, I first modeled the game in the form of an Excel spreadsheet to manipulate the vehicles and begin to develop a strategy to solve the problem. I then modeled the game in python thanks to the "pygame" library: board, vehicles (trucks and cars), moves, collisions between vehicles and with the board borders, victory situation. The only thing left to do then was to test solving algorithms for this kind of problems : I turned to the graphs domain.

The objective of this version remains the same as in the original game (make the red car escape) but my solution must :
- Respect a minimum number of moves of the vehicles,
- Be found in a reasonable amount of time for a computer (not more than one second),
- Exist whatever the configuration of the board in its basic size (6x6 squares).

In the "extended" version of my program, I would have liked to implement :
- A resolution whatever the size of the board,
- A generation of the placement of the cars according to different difficulties (placing more or less vehicles),
- Another resolution algorithm to compare the results in terms of performance (time, CPU usage, RAM usage),
- A recognition of the unsolvable boards (because of the position of the vehicles).

I have found a solution that works: it consists in generating all the possible game boards with, for each board, only one vehicle having been moved. For each board thus built, we check that it represents a valid situation (a vehicle trying to move on another vehicle is not a valid situation for example) and that the board has not already been generated. We continue in this way, taking care that each new valid tray generated is linked to the previous tray (the one from which this new tray was generated via the movement of a vehicle) and we stop only when a tray representing a victory situation is generated (red car out of the tray) or when there are no more trays that can be generated (all the movements of all the vehicles have been represented in the form of a tray, but none of them has created a victory situation).
If a board representing a victory is found, the moves (applied to the vehicles) that led to that victory board are identified by going from generated board to generated board back to the original board of the game.
